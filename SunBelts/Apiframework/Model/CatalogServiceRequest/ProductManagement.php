<?php

/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Sunbelts\Apiframework\Model\CatalogServiceRequest;

use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Catalog\Setup\CategorySetupFactory;
use Magento\Eav\Model\Entity\Attribute\SetFactory as AttributeSetFactory;
use Magento\Eav\Model\Config;
use \Sunbelts\Apiframework\Model\Request;

class ProductManagement implements \Sunbelts\Apiframework\Api\ProductManagementInterface {

    protected $_request;
    private $eavSetupFactory;
    private $attributeSetFactory;
    private $categorySetupFactory;
    protected $eavConfig;
    protected $_productCollectionFactory;
    protected $_productloader;
    protected $_requestsunbelt;

    /**
     * scope config 
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    const CURL_POST = true;
    const CURL_PUT = true;
    const CURL_RETURNTRANSFER = true;
    const CURL_NOPROGRESS = false;
    const CURL_TIMEOUT = 60;
    const CURL_VERBOSE = true;
    const SUCCESS = 200;
    const CLIENTSECRET = 'sunbelt/sunbelt_catalog/clientSecret';
    const CLIENTKEY = 'sunbelt/sunbelt_catalog/clientkey';
    const CHANNEL = 'sunbelt/sunbelt_catalog/channel';
    const COMPANYID = 'sunbelt/sunbelt_catalog/companyID';
    const ENDPOINT = 'sunbelt/sunbelt_endpoint/product_endpoint';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param DataObjectHelper $dataObjectHelper
     * @param \Sunbelts\Apiframework\Model\ResourceModel\Request $resource
     * @param \Sunbelts\Apiframework\Model\ResourceModel\Request\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
            \Sunbelts\Apiframework\Model\Request\CurlRequest $curlrequest,
            \Magento\Framework\Serialize\Serializer\Json $json,
            \Magento\Framework\Json\Helper\Data $jsonHelper,
            EavSetupFactory $eavSetupFactory,
            AttributeSetFactory $attributeSetFactory,
            CategorySetupFactory $categorySetupFactory,
            ModuleDataSetupInterface $setup,
            Config $eavConfig,
            \Magento\Framework\ObjectManagerInterface $objectmanager,
            \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
            \Magento\Catalog\Model\ResourceModel\Eav\Attribute $attributeFactory,
            \Magento\Catalog\Model\ProductFactory $_productloader,
            \Sunbelts\Apiframework\Model\Request $_requestsunbelt,
            \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
    ) {
        $this->curlrequest = $curlrequest;
        $this->json = $json;
        $this->jsonHelper = $jsonHelper;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->attributeSetFactory = $attributeSetFactory;
        $this->categorySetupFactory = $categorySetupFactory;
        $this->setup = $setup;
        $this->eavConfig = $eavConfig;
        $this->_objectmanager = $objectmanager;
        $this->_attributeFactory = $attributeFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_productloader = $_productloader;
        $this->_requestsunbelt = $_requestsunbelt;
    }

    /**
     * {@inheritdoc}
     */
    public function getProduct($productId) {
        $clientSecret = $this->_scopeConfig->getValue(self::CLIENTSECRET, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $clientkey = $this->_scopeConfig->getValue(self::CLIENTKEY, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $channel = $this->_scopeConfig->getValue(self::CHANNEL, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $companyID = $this->_scopeConfig->getValue(self::COMPANYID, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $endpoint = $this->_scopeConfig->getValue(self::ENDPOINT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $endpoint = $endpoint . $productId;
        $url = $this->_requestsunbelt;
        $header = array("clientSecret:" . $clientSecret, "clientKey:" . $clientkey, "channel:" . $channel, "companyID:" . $companyID);
        $response = $url->sendRequest($endpoint, $header);
        $jd = $this->jsonHelper->jsonDecode($response);
        foreach ($jd as $datass) {
            foreach ($datass as $value) {
                foreach ($value as $key => $subvalue) {
                    if (!empty($this->listAttributeSet())) {
                        $this->create_update_attribute($key);
                    } else {
                        $this->create_update_attributeset();
                        $this->create_update_attribute($key);
                    }
                }
            }
        } return $this->setJsonProductData($jd) . $response;
    }

    public function getProductCollection() {
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        return $collection;
    }

    public function listAttributeSet() {
        $resource = $this->_objectmanager->get('Magento\Framework\App\ResourceConnection');
        $connection = $resource->getConnection();
        $tableName = $resource->getTableName('eav_attribute_set'); //gives table name with prefix
        $sql = "Select * FROM " . $tableName . " Where attribute_set_name='SBR_ATTRIBUTE_SET'";
        $resultset = $connection->fetchAll($sql);
        return $resultset;
    }

    public function listAttributeCodes() {
        $attributeInfo = $this->_attributeFactory->getCollection();
        foreach ($attributeInfo as $attributes) {
            $attributeId[] = strtolower($attributes->getAttributeCode());
        }
        return $attributeId;
    }

    public function create_update_attributeset() {
        $categorySetup = $this->categorySetupFactory->create(['setup' => $this->setup]);
        $attributeSet = $this->attributeSetFactory->create();
        $entityTypeId = $categorySetup->getEntityTypeId(\Magento\Catalog\Model\Product::ENTITY);
        $attributeSetId = $categorySetup->getDefaultAttributeSetId($entityTypeId);
        $data = [
            'attribute_set_name' => 'SBR_ATTRIBUTE_SET',
            'entity_type_id' => $entityTypeId,
            'sort_order' => 200,
        ];
        $attributeSet->setData($data);
        $attributeSet->validate();
        $attributeSet->save();
        $attributeSet->initFromSkeleton($attributeSetId);
        $attributeSet->save();
    }

    public function create_update_attribute($key) {
        $this->setup->startSetup();
        $attcodes = $this->listAttributeCodes();
        $keystr = strtolower($key);
        if (in_array($keystr, $attcodes)) {
            return "Attribute code already exists";
        } else {
            $eavSetup = $this->eavSetupFactory->create(['setup' => $this->setup]);
            $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Product::ENTITY,
                    $keystr,
                    [
                        'type' => 'varchar',
                        'backend' => '',
                        'frontend' => '',
                        'label' => $key,
                        'input' => 'text',
                        'class' => '',
                        'source' => '',
                        'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible' => true,
                        'required' => false,
                        'user_defined' => false,
                        'default' => '',
                        'searchable' => false,
                        'filterable' => false,
                        'comparable' => false,
                        'visible_on_front' => false,
                        'used_in_product_listing' => true,
                        'unique' => false,
                        'apply_to' => ''
                    ]
            );
            $this->setup->endSetup();
        }
        return "Attribute Set updated";
    }

    public function createProduct($value) {
        $_product = $this->_productloader->create();
        $productId = $value['Id'];
        if ($productId != "") {
            $_product = $_product->load($productId);
        }
        $_product->setSku($value['Id']); // Set your sku here
        $_product->setName($value['Name']); // Name of Product
        $_product->setAttributeSetId(4); // Attribute set id
        $_product->setStatus(1); // Status on product enabled/ disabled 1/0
        $_product->setWeight(10); // weight of product
        $_product->setVisibility(4); // visibilty of product (catalog / search / catalog, search / Not visible individually)
        $_product->setTaxClassId(0); // Tax class id
        $_product->setTypeId('simple'); // type of product (simple/virtual/downloadable/configurable)
        $_product->setPrice(str_replace('$', '', $value['prices']['dayPrice'])); // price of product
        $_product->setDescription($value['Description']);
        $_product->setVideourls("");
        $_product->setIsRentableOnWebsite($value['IsRentableOnWebsite']);
        $_product->setWhatsNotIncludedWithItem($value['WhatsNotIncludedWithItem']);
        $_product->setWhatsIncludedWithItem($value['WhatsIncludedWithItem']);
        $_product->setFeatures($value['Features'][0]);
        $_product->setIsOverWeight($value['IsOverWeight']);
        $_product->setIsrapidreturneligible('Israpidreturneligible');
        $_product->setIsConsumerItem($value['IsConsumerItem']);
        $_product->setIsPopularItem($value['IsPopularItem']);
        $_product->setMakemodels("");
        $_product->setSafetyItems($value['SafetyItems'][0]);
        $_product->setImageurls($value['ImageURLs'][0]);
        $_product->setCatclass($value['CatClass']);
        $_product->setWynnecat($value['WynneCAT']);
        $_product->setWynneclass($value['WynneCLASS']);
        $_product->setRelativeurl($value['RelativeURL']);
        $_product->setFullurl($value['FullURL']);
        $_product->setHighlightshtml($value['HighlightsHtml']);
        $_product->setStockData(
                array(
                    'use_config_manage_stock' => 0,
                    'manage_stock' => 0,
                    'is_in_stock' => 1,
                    'qty' => 999999999
                )
        );
        $_product->save();
        exit('Inserted!');
    }

    public function setJsonProductData($jsonData) {
        $sbrid = array();
        $idArray = array();
        foreach ($jsonData as $datass) {
            foreach ($datass as $value) {
                foreach ($value as $key => $subvalue) {
                    if (!in_array($value['Id'], $idArray)) {
                        $idArray[] = $value['Id'];
                    }
                }
            }
        }
        $productCollection = $this->getProductCollection();
        $url = "http://sunbelt.magento.com:8080/rest";
        $token_url = $url . "/V1/integration/admin/token";
        $username = "admin";
        $password = "test123#";
        $ch = curl_init();
        $data = array("username" => $username, "password" => $password);
        $data_string = json_encode($data);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $token_url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        $token = curl_exec($ch);
        $adminToken = json_decode($token);
        $headers = array('Content-Type:application/json', 'Authorization:Bearer ' . $adminToken);
        // Createt Product REST API URL
        $apiUrl = $url . "/V1/products";
        $ch = curl_init();
        foreach ($productCollection as $product) {
            $id = $product->getId();
            if (in_array($id, $idArray)) {
                foreach ($jsonData as $datass) {
                    foreach ($datass as $value) {
                        foreach ($value as $key => $subvalue) {
                            $this->createProduct($value);
                        }
                    }
                }
            }
        }
    }

}
