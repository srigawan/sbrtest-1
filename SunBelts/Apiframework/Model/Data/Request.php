<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Sunbelts\Apiframework\Model\Data;

use Sunbelts\Apiframework\Api\Data\RequestInterface;

class Request extends \Magento\Framework\Api\AbstractExtensibleObject implements RequestInterface
{

    /**
     * Get request_id
     * @return string|null
     */
    public function getRequestId()
    {
        return $this->_get(self::REQUEST_ID);
    }

    /**
     * Set request_id
     * @param string $requestId
     * @return \Sunbelts\Apiframework\Api\Data\RequestInterface
     */
    public function setRequestId($requestId)
    {
        return $this->setData(self::REQUEST_ID, $requestId);
    }

    /**
     * Get CurlRequest
     * @return string|null
     */
    public function getCurlRequest()
    {
        return $this->_get(self::CURLREQUEST);
    }

    /**
     * Set CurlRequest
     * @param string $curlRequest
     * @return \Sunbelts\Apiframework\Api\Data\RequestInterface
     */
    public function setCurlRequest($curlRequest)
    {
        return $this->setData(self::CURLREQUEST, $curlRequest);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Sunbelts\Apiframework\Api\Data\RequestExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Sunbelts\Apiframework\Api\Data\RequestExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Sunbelts\Apiframework\Api\Data\RequestExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

