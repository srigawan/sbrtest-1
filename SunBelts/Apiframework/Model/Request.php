<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Sunbelts\Apiframework\Model;

use Magento\Framework\Api\DataObjectHelper;
use Sunbelts\Apiframework\Api\Data\RequestInterface;
use Sunbelts\Apiframework\Api\Data\RequestInterfaceFactory;

use Exception;
use Magento\Eav\Api\Data\AttributeSetInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Catalog\Api\AttributeSetRepositoryInterface;


class Request extends \Magento\Framework\Model\AbstractModel
{
/**
     * @var AttributeSetRepositoryInterface
     */
    private $attributeSetRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    
    protected $_request;

    public $_objectmanager;

    protected $requestDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'sunbelts_apiframework_request';
    protected $_productCollectionFactory;

    const CURL_POST = true;
    const CURL_PUT = true;
    const CURL_RETURNTRANSFER = true;
    const CURL_NOPROGRESS = false;
    const CURL_TIMEOUT = 60;
    const CURL_VERBOSE = true;
    const SUCCESS = 200;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param RequestInterfaceFactory $requestDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Sunbelts\Apiframework\Model\ResourceModel\Request $resource
     * @param \Sunbelts\Apiframework\Model\ResourceModel\Request\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
       // \Sunbelts\Apiframework\Logger $salogger,
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        RequestInterfaceFactory $requestDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Sunbelts\Apiframework\Model\ResourceModel\Request $resource,
        \Sunbelts\Apiframework\Model\ResourceModel\Request\Collection $resourceCollection,
        \Sunbelts\Apiframework\Model\Request\CurlRequest $curlrequest,
        \Magento\Framework\Serialize\Serializer\Json $json,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        AttributeSetRepositoryInterface $attributeSetRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\ObjectManagerInterface $objectmanager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,        



        array $data = []
    ) {
       // $this->_salogger = $salogger;
        $this->curlrequest = $curlrequest;
        $this->requestDataFactory = $requestDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->json = $json;
        $this->jsonHelper = $jsonHelper;
        $this->attributeSetRepository = $attributeSetRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->_objectmanager = $objectmanager;
        $this->_productCollectionFactory = $productCollectionFactory;    

    }
    
    

    /**
     * Retrieve request model with request data
     * @return RequestInterface
     */
    public function sendRequest($url, $header)
    {
        $request = $this->_getRequest();
        $request->setUrl($url);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        $result1 = curl_exec($ch);
        
        return $result1;
    }
 

    private function _getRequest()
    {
        if ($this->_request === null) {
            $this->_request = $this->curlrequest;
        }
        return $this->_request;
    }
 
 public function getDataModel()
    {
        $requestData = $this->getData();
        
        $requestDataObject = $this->requestDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $requestDataObject,
            $requestData,
            RequestInterface::class
        );
        
        return $requestDataObject;
    }
}

