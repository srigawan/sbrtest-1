<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Sunbelts\Apiframework\Api;

interface ProductManagementInterface
{

    /**
     * GET for Product api
     * @param string $productId
     * @return string
     */
    public function getProduct($productId);
}

