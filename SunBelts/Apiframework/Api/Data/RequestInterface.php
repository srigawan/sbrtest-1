<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Sunbelts\Apiframework\Api\Data;

interface RequestInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const REQUEST_ID = 'request_id';
    const CURLREQUEST = 'CurlRequest';

    /**
     * Get request_id
     * @return string|null
     */
    public function getRequestId();

    /**
     * Set request_id
     * @param string $requestId
     * @return \Sunbelts\Apiframework\Api\Data\RequestInterface
     */
    public function setRequestId($requestId);

    /**
     * Get CurlRequest
     * @return string|null
     */
    public function getCurlRequest();

    /**
     * Set CurlRequest
     * @param string $curlRequest
     * @return \Sunbelts\Apiframework\Api\Data\RequestInterface
     */
    public function setCurlRequest($curlRequest);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Sunbelts\Apiframework\Api\Data\RequestExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Sunbelts\Apiframework\Api\Data\RequestExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Sunbelts\Apiframework\Api\Data\RequestExtensionInterface $extensionAttributes
    );
}

