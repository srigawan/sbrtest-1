<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Sunbelts\Apiframework\Api\Data;

interface RequestSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Request list.
     * @return \Sunbelts\Apiframework\Api\Data\RequestInterface[]
     */
    public function getItems();

    /**
     * Set CurlRequest list.
     * @param \Sunbelts\Apiframework\Api\Data\RequestInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

