<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Sunbelts\Apiframework\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface RequestRepositoryInterface
{

    /**
     * Save Request
     * @param \Sunbelts\Apiframework\Api\Data\RequestInterface $request
     * @return \Sunbelts\Apiframework\Api\Data\RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Sunbelts\Apiframework\Api\Data\RequestInterface $request
    );

    /**
     * Retrieve Request
     * @param string $requestId
     * @return \Sunbelts\Apiframework\Api\Data\RequestInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($requestId);

    /**
     * Retrieve Request matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Sunbelts\Apiframework\Api\Data\RequestSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Request
     * @param \Sunbelts\Apiframework\Api\Data\RequestInterface $request
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Sunbelts\Apiframework\Api\Data\RequestInterface $request
    );

    /**
     * Delete Request by ID
     * @param string $requestId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($requestId);
}

