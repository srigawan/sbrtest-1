<?php
/**
 * @copyright 2020 Sapient
 */
namespace Sunbelts\Apiframework\Logger;

use Monolog\Logger;

class ApiFrameworkHandler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name
     * @var string
     */
    protected $fileName = '/var/log/apiframework.log';
}
