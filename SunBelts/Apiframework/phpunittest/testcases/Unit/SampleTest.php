<?php
 
namespace SunBelts\Apiframework\phpunittest\testcases\Unit;
 
use SunBelts\Apiframework\phpunittest\Block\SampleClass;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;
 
class SampleTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Inchoo\Testing\Block\SampleClass
     */
    protected $sampleClass;
 
    /**
     * @var string
     */
    protected $expectedMessage;
 
    public function setUp(): void
    {
    $objectManager = new ObjectManager($this);
    $this->sampleClass = $objectManager->getObject('SunBelts\Apiframework\phpunittest\Block\SampleClass');
        $this->expectedMessage = 'Hello, this is sample test';
    }
 
    public function testGetMessage()
    {
        $this->assertEquals($this->expectedMessage, $this->sampleClass->getMessage());
    }
 
}